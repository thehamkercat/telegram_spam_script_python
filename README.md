# Telegram Spam Script

[![forthebadge](http://forthebadge.com/images/badges/made-with-python.svg)](http://forthebadge.com)


## Installation
```bash
pip3 install telethon
git clone https://github.com/thehamkercat/telegram_spam_script_python.git
cd telegram_spam_script_python
```
Fill your *api_id* and *api_hash* in `spam.py`, also replace the spam message with what you want to spam and then execute. 

```bash
./run.sh
```

### Note
- This script is only meant to be executed on high performance systems so you may feel slow if your pc doesn't have enough resources.
- Python3 and pip3 is necessary for this script to work, install them with:

```bash
sudo apt-get install python3 python3-pip
```
### Warning!
-This is only for literal educational purposes, so only use this script in education related groups. 

<img src="https://hamker.h4ck.me/Index/a.gif" alt="alext" width="193" height="193">
